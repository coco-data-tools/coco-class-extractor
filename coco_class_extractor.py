import json
import argparse

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    
    parser.add_argument('-i', '--input', required=True, help='Input Annotation File')
    parser.add_argument('-o', '--output', required=True, help='New Annotation File')
    parser.add_argument('-c', '--classes', required=True, help='Classes', action='append')

    args = parser.parse_args()

    input_file = args.input
    output_file = args.output
    classes = args.classes

    new_classes = []
    new_annotations = []
    new_images = []
    class_ids = []
    file_names = []

    print(input_file)
    print(classes)

    print("Opening json file...")

    with open(input_file, "r") as json_data:
        data = json.loads(json_data.read())
    
    print("Searching categories...")

    for i in range(len(data['categories'])):
        #print(data['categories'][i]['name'])
        if data['categories'][i]['name'] in classes:
            new_classes.append(data['categories'][i])
            class_ids.append(data['categories'][i]['id'])

    print("Searching annotations...")

    for i in range(len(data['annotations'])):
        if data['annotations'][i]['category_id'] in class_ids:
            new_annotations.append(data['annotations'][i])
            file_names.append(data['annotations'][i]['image_id'])

    print("Searching images...")

    for i in range(len(data['images'])):
        if data['images'][i]['id'] in file_names:
            new_images.append(data['images'][i])
            
    data['categories'] = new_classes
    data['annotations'] = new_annotations
    data['images'] = new_images

    print("Populating new json")

    with open(output_file, "w") as out:
        json.dump(data, out)

    print("Finished")



