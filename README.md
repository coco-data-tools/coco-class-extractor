# Coco Class Extractor

Generate a new annotation file containing only specified classes

### Installation

_git clone https://gitlab.com/coco-data-tools/coco-class-extractor.git_

### Usage

To extract a specific class, for example 'person':

_python coco_class_extractor.py -i path/to/annotation.json -o path/to/new/annotation.json -c "person"_

To extract multiple classes, for example 'person' and 'handbag'

_python coco_class_extractor.py -i path/to/annotation.json -o path/to/new/annotation.json -c "person" -c "handbag"_
